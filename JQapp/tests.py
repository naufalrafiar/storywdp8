rom django.test import TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import LiveServerTestCase

import time
import unittest
# Create your tests here.

class Functional_test_for_accordion_jquery(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.maximize_window()
        self.browser.implicitly_wait(20)
    def tearDown(self):
        self.browser.quit()
        super().tearDown()

    def test_landing_page(self):
        self.browser.get(self.live_server_url + '/')
        self.assertIn('Hello', self.browser.title)
        self.assertIn('U: Up', self.browser.page_source)
        time.sleep(3)

    def test_display(self):
        self.browser.get(self.live_server_url + '/')
        about = self.browser.find_element_by_css_selector('li:nth-child(1) > .accordion')
        exp = self.browser.find_element_by_css_selector('li:nth-child(2) > .accordion')
        contact = self.browser.find_element_by_css_selector('li:nth-child(3) > .accordion')
        juke = self.browser.find_element_by_css_selector('li:nth-child(4) > .accordion')

        about.click()
        exp.click()
        contact.click()
        juke.click()

        self.assertIn('Naufal', self.browser.page_source)
        self.assertIn('Achievements', self.browser.page_source)
        self.assertIn('Organizational', self.browser.page_source)
        self.assertIn('Hobbies', self.browser.page_source)

