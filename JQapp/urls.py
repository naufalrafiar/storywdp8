from django.urls import path, include
from .views import Index

app_name = 'JQapp'

urlpatterns = [
    path('', Index, name='Index'),
]